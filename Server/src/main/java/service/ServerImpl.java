package service;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import proto.SenderGrpc;
import proto.Server;

public class ServerImpl extends SenderGrpc.SenderImplBase {
    @Override
    public void getNameCNP(Server.theNameCNP request, StreamObserver<Empty> responseObserver) {
       // super.getNameCNP(request, responseObserver);
        System.out.println(request);
        System.out.println("Name:"+request.getName());

        if(request.getCNP().startsWith("1") || request.getCNP().startsWith("5")){
            System.out.println("Gender:M");
        }
        else {
            System.out.println("Gender:F");
        }

        if(request.getCNP().startsWith("1") || request.getCNP().startsWith("2")) {
            int age=21+2000-(1900+Integer.parseInt(request.getCNP().substring(1,3)));
            System.out.print("Age:"+age);
        }
        else {
            int age=21-Integer.parseInt(request.getCNP().substring(1,3));
            System.out.print("Age:"+age);
        }
    }
}
