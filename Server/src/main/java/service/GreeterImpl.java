package service;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import proto.SenderGrpc;
import proto.Server;

public class GreeterImpl extends SenderGrpc.SenderImplBase {
    @Override
    public void getNameCNP(Server.theNameCNP request, StreamObserver<Empty> responseObserver) {
        super.getNameCNP(request, responseObserver);
    }
}
