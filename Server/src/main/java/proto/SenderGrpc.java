package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition. 
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: Server.proto")
public final class SenderGrpc {

  private SenderGrpc() {}

  public static final String SERVICE_NAME = "Sender";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.Server.theNameCNP,
      com.google.protobuf.Empty> getGetNameCNPMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetNameCNP",
      requestType = proto.Server.theNameCNP.class,
      responseType = com.google.protobuf.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.Server.theNameCNP,
      com.google.protobuf.Empty> getGetNameCNPMethod() {
    io.grpc.MethodDescriptor<proto.Server.theNameCNP, com.google.protobuf.Empty> getGetNameCNPMethod;
    if ((getGetNameCNPMethod = SenderGrpc.getGetNameCNPMethod) == null) {
      synchronized (SenderGrpc.class) {
        if ((getGetNameCNPMethod = SenderGrpc.getGetNameCNPMethod) == null) {
          SenderGrpc.getGetNameCNPMethod = getGetNameCNPMethod = 
              io.grpc.MethodDescriptor.<proto.Server.theNameCNP, com.google.protobuf.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Sender", "GetNameCNP"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.Server.theNameCNP.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.google.protobuf.Empty.getDefaultInstance()))
                  .setSchemaDescriptor(new SenderMethodDescriptorSupplier("GetNameCNP"))
                  .build();
          }
        }
     }
     return getGetNameCNPMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SenderStub newStub(io.grpc.Channel channel) {
    return new SenderStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SenderBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new SenderBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SenderFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new SenderFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static abstract class SenderImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *rpc numefunctie (ce trimite_Clientul) returns (ce_Trimite_Serverul)
     * </pre>
     */
    public void getNameCNP(proto.Server.theNameCNP request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnimplementedUnaryCall(getGetNameCNPMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetNameCNPMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.Server.theNameCNP,
                com.google.protobuf.Empty>(
                  this, METHODID_GET_NAME_CNP)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class SenderStub extends io.grpc.stub.AbstractStub<SenderStub> {
    private SenderStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SenderStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SenderStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SenderStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc numefunctie (ce trimite_Clientul) returns (ce_Trimite_Serverul)
     * </pre>
     */
    public void getNameCNP(proto.Server.theNameCNP request,
        io.grpc.stub.StreamObserver<com.google.protobuf.Empty> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetNameCNPMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class SenderBlockingStub extends io.grpc.stub.AbstractStub<SenderBlockingStub> {
    private SenderBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SenderBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SenderBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SenderBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc numefunctie (ce trimite_Clientul) returns (ce_Trimite_Serverul)
     * </pre>
     */
    public com.google.protobuf.Empty getNameCNP(proto.Server.theNameCNP request) {
      return blockingUnaryCall(
          getChannel(), getGetNameCNPMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition. 
   * </pre>
   */
  public static final class SenderFutureStub extends io.grpc.stub.AbstractStub<SenderFutureStub> {
    private SenderFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private SenderFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SenderFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new SenderFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     *rpc numefunctie (ce trimite_Clientul) returns (ce_Trimite_Serverul)
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.google.protobuf.Empty> getNameCNP(
        proto.Server.theNameCNP request) {
      return futureUnaryCall(
          getChannel().newCall(getGetNameCNPMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_NAME_CNP = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SenderImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SenderImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_NAME_CNP:
          serviceImpl.getNameCNP((proto.Server.theNameCNP) request,
              (io.grpc.stub.StreamObserver<com.google.protobuf.Empty>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SenderBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SenderBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.Server.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Sender");
    }
  }

  private static final class SenderFileDescriptorSupplier
      extends SenderBaseDescriptorSupplier {
    SenderFileDescriptorSupplier() {}
  }

  private static final class SenderMethodDescriptorSupplier
      extends SenderBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SenderMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SenderGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SenderFileDescriptorSupplier())
              .addMethod(getGetNameCNPMethod())
              .build();
        }
      }
    }
    return result;
  }
}
