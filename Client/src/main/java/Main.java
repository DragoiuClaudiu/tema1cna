import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.SenderGrpc;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();


        SenderGrpc.SenderBlockingStub bookStub = SenderGrpc.newBlockingStub(channel);

        channel.shutdown();
    }
}
